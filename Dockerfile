FROM ubuntu:latest
MAINTAINER Khalid Almufti "khalid.almufti@globaleagle.com"

# adding 32bit architecture and installing snx dependencies, with some clean-up to keep image small
RUN dpkg --add-architecture i386 && apt-get update && apt-get install -y --no-install-recommends \
    bzip2 \
    kmod \
    libstdc++5:i386\
    libpam0g:i386 \
    libx11-6:i386 \
    iputils-ping \
    openssh-client \
    iproute2 \
 && rm -rf /var/lib/apt/lists/*

# copy shell script to easy using
COPY dsnx /opt/dsnx

# some kernel modules needed the vpn tunnel to work
COPY 4.9.60-linuxkit-aufs /lib/modules/4.9.60-linuxkit-aufs

# installing snx and clean-up
COPY snx_install.sh /tmp
RUN /tmp/snx_install.sh && rm /tmp/snx_install.sh

# setting up default working directory
WORKDIR /root

# setting up image volume dir
VOLUME ["/root/"]

# setting shell script as the main app for the image
ENTRYPOINT ["/opt/dsnx"]
